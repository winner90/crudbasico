﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDbasico
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("OPCIONES CRUD:");
            Console.WriteLine("==============\n");
            Console.WriteLine("[1] Crear persona");
            Console.WriteLine("[2] ver persona");
            Console.WriteLine("[3] ver todas las personas");
            Console.WriteLine("[4] Actualizar persona");
            Console.WriteLine("[5] borar persona");
            Console.WriteLine("[6] salir");
            Console.Write("\nSelección: ");
            var s = Console.ReadLine();

            using(var db = new PersonaContext())
            {
                int id = 0;
                switch(s)
                {
                    case "1":
                    CrearPersona(db);
                    break;

                    case "2":
                    Console.WriteLine("Indique el ID de la persona: ");
                    id = Convert.ToInt16(Console.ReadLine());
                    VerPersona(db, id);
                    break;

                    case "3":
                    VerTodasPersona(db);
                    break;

                    case "4":
                    Console.Write("Indique el ID de la persona: ");
                    id = Convert.ToInt16(Console.ReadLine());
                    ActualizarPersona(db, id);
                    break;

                    case "5":
                    Console.Write("Indique el ID de la persona: ");
                    id = Convert.ToInt16(Console.ReadLine());
                    BorrarPersona(db, id);
                    break;

                    case "6":
                    Environment.Exit(0);
                    break;
                }
            }

            Console.ReadLine();
        }

        static void CrearPersona(PersonaContext db)
        {
            var persona = new Persona();

            Console.Write("Nombre: ");
            persona.Nombre = Console.ReadLine();

            Console.Write("Apellido: ");
            persona.Apellido = Console.ReadLine();

            Console.Write("Edad: ");
            persona.Edad = Convert.ToInt16(Console.ReadLine());

            db.Personas.Add(persona);

            db.SaveChanges();

            Console.WriteLine("Datos guardados!!!");
        }
        static void VerPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                Console.WriteLine("Nombre: " + item.Nombre);
                Console.WriteLine("Apellido: " + item.Apellido);
                Console.WriteLine("Edad: " + item.Edad);
            }

        }
        static void VerTodasPersona(PersonaContext db)
        {
            var query = from q in db.Personas
                        select q;

            foreach(var item in query)
            {
                Console.WriteLine("Id: " + item.Id);
                Console.WriteLine("Nombre: " + item.Nombre);
                Console.WriteLine("Apellido: " + item.Apellido);
                Console.WriteLine("Edad: " + item.Edad);
                Console.WriteLine("\n");
            }

        }
        static void ActualizarPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                Console.WriteLine("Nombre actual: " + item.Nombre);
                Console.Write("Nombre nuevo: ");
                item.Nombre = Console.ReadLine();
                Console.WriteLine("\n");

                Console.WriteLine("Apellido actual: " + item.Apellido);
                Console.Write("Apellido nuevo: ");
                item.Apellido = Console.ReadLine();
                Console.WriteLine("\n");

                Console.WriteLine("Edad actual: " + item.Edad);
                Console.Write("Edad nuevo: ");
                item.Edad = Convert.ToInt16(Console.ReadLine());
                Console.WriteLine("\n");
            }

            db.SaveChanges();

            Console.WriteLine("Datos actualizados!!!");
        }
        static void BorrarPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                db.Personas.Remove(item);
            }

            db.SaveChanges();

            Console.WriteLine("Registro eliminado!!!");
        }
    }
}
